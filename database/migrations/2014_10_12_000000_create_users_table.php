<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {
      $table->id();
      $table->string('name');
      $table->string('password');
      $table->string('codeRCCM')->nullable();
      $table->string('codeNINEA')->nullable();
      $table->string('email')->unique();
      $table->string('token')->nullable(false);
      $table->boolean('station')->default(false);
      $table->boolean('admin')->default(false);
      $table->boolean('activated')->default(false);
      $table->boolean('verified')->default(false);
      $table->rememberToken();
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('users');
  }
}
