<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::namespace('Api')->group(function(){
	Route::namespace('Users')->group(function(){
		Route::prefix('v1/account')->group(function(){

			Route::post('login','LoginController@login');
			Route::post('register','RegisterController@register');

			Route::middleware('auth:api')->group(function(){
				Route::get('logout', 'LogoutController@logout');
			});

		});
	});

	Route::namespace('Dashboard')->group(function(){
		Route::prefix('v1/account')->group(function(){

			Route::middleware('auth:api')->group(function(){
				Route::apiResource('station','StationCRUDController');
			});

		});
	});

});
