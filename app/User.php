<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use HasApiTokens, Notifiable;

  protected $guarded = [];
  
  protected $hidden = [
    'password', 
    'remember_token', 
    'admin', 
    'activated',
    'verified',
  ];

}
