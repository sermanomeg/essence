<?php

namespace App\Http\Resources\Users;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Http\Resources\Json\JsonResource;

class RegisterResource extends JsonResource
{
  public function toArray($request)
  {
    $token    = $this->createToken('144XX@');
    $created  = Carbon::parse($this->creates_at);
    $updated  = Carbon::parse($this->updated_at);
    $expiredT = Carbon::parse($token->token->expires_at);

    $Token = [
      'typeToken'   => 'Bearer',
      'accessToken' => $token->accessToken,
      'expired'     => $expiredT->toDateTimeString(),
    ];


    $data = [
      'id'        => $this->id,
      'station'   => (int) $this->station,
      'name'      => $this->name,
      'email'     => $this->email,
      'codeRCCM'  => $this->codeRCCM,
      'codeNINEA' => $this->codeNINEA, 
      'token'     => $Token,
      'created'   => $created->toDateTimeString(),
      'updated'   => $updated->toDateTimeString(),
    ];

    return [
      'code'   => '00',
      'status' => 'Success',
      'data'   => $data,
    ];
  }
}
