<?php

namespace App\Http\Resources\Dashboard;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerCRUDResource extends JsonResource
{
  public function toArray($request)
  {
    $created = Carbon::parse($this->creates_at);
    $updated = Carbon::parse($this->updated_at);

    $data = [
      'id'        => $this->id,
      'station'   => (int) $this->station,
      'name'      => $this->name,
      'email'     => $this->email,
      'codeRCCM'  => $this->codeRCCM,
      'codeNINEA' => $this->codeNINEA, 
      'created'   => $created->toDateTimeString(),
      'updated'   => $updated->toDateTimeString(),
    ];
    
    return [
      'code'   => '00',
      'status' => 'Success',
      'data'   => $data,
    ];
  }
}
