<?php

namespace App\Http\Requests\Api\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class CustomerCRUDRequest extends FormRequest
{
  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    return [
      'name'      => ['nullable','string','max:255'],
      'codeRCCM'  => ['nullable','string','max:255'],
      'codeNINEA' => ['nullable','string','max:255'],
      'email'     => ['nullable','string','email:dns,filter','max:255'],
    ];
  }
}
