<?php

namespace App\Http\Requests\Api\Users;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    return [
      'station'   => ['nullable','boolean'],
      'name'      => ['required','string','max:255'],
      'password'  => ['required','string','min:10','max:255','confirmed'],
      'codeRCCM'  => ['required','string','max:255'],
      'codeNINEA' => ['required','string','max:255'],
      'email'     => ['required','string','email:dns,filter','max:255'],
    ];
  }
}
