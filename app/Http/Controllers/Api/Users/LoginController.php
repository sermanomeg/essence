<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Resources\Users\RegisterResource;
use App\Http\Requests\Api\Users\LoginRequest;

class LoginController extends Controller
{
  public function login(LoginRequest $request)
  {
		$validate = $request->validated();
		$validate = Arr::except($validate, 'customer');
		$auth     = auth()->attempt($validate);

		if ($auth) {
			$user = $request->user();

			if ($user->station == $request->station) {
				$data = RegisterResource::make($user);
				return response()->json($data, 200);
			} else {

				$data = ['message' => 'Unauthorized'];
				return response()->json($data, 401);
			}
		} 

		$errors = [
			'code'   => '1001',
			'status' => 'Failed',
		];

		return response()->json($errors, 400);
  }

}
