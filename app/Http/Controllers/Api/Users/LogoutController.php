<?php

namespace App\Http\Controllers\Api\Users;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
  public function logout(Request $request)
  {
  	$user = $request->user();
  	$user->token()->revoke();

  	$data = [
			"code"   => "00",
			"status" => "Success",
  	];

  	return response()->json($data, 200);
  }
}
