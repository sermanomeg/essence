<?php

namespace App\Http\Controllers\Api\Users;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;
use App\Http\Resources\Users\RegisterResource;
use App\Http\Requests\Api\Users\RegisterRequest;

class RegisterController extends Controller
{
	public function register(RegisterRequest $request)
	{
		$validate = $request->validated();
		$password = Hash::make($validate['password']);
		$validate = Arr::except($validate,'password');
		$validate = Arr::add($validate,'password', $password);
		$validate = Arr::add($validate,'token', Str::random(20));

		try {
			$user = User::create($validate);
			$data = RegisterResource::make($user);

			return response()->json($data, 201);

		} catch (QueryException $e) {
			$errors = [
				"code"    => $e->errorInfo[0],
				"status"  => "Failed",
				"message" => $e->errorInfo[2],
			];

			return response()->json($errors, 400);
		}
	}

}
