<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Dashboard\CustomerCRUDResource;
use App\Http\Requests\Api\Dashboard\CustomerCRUDRequest;

class StationCRUDController extends Controller
{
  public function index()
  {
    $user = auth()->user();
    $data = CustomerCRUDResource::make($user);

    return response()->json($data, 200);
  }

  public function show($id)
  {
    if (auth()->user()->id == $id) {

      $user = User::findOrFail($id);
      $data = CustomerCRUDResource::make($user);

      return response()->json($data, 201);
    }

    $data = ['message' => 'Unauthorized'];
    return response()->json($data, 401);
  }

  public function update(CustomerCRUDRequest $request, $id)
  {
    if (auth()->user()->id == $id) {

      $user = User::find($id);
      $user->activated = false;
      $user->update($request->validated());
      $user->save();
      $data = CustomerCRUDResource::make($user);

      return response()->json($data, 201);
    }

    $data = ['message' => 'Unauthorized'];
    return response()->json($data, 401);
  }
}
