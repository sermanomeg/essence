<?php

namespace App\Http\Middleware\Api\Users;

use Closure;

class StationMiddleware
{
  public function handle($request, Closure $next)
  {
    if (!$request->user()->station) {
      return response()->json(['message' => 'Unauthorized'], 401);
    }

    return $next($request);
  }
}
