<?php

namespace App\Http\Middleware\Api\Users;

use Closure;

class VerifiedMiddleware
{
  public function handle($request, Closure $next)
  {
    if (!$request->user()->verified) {
      return response()->json(['message' => 'Unauthorized'], 401);
    }

    return $next($request);
  }
}
