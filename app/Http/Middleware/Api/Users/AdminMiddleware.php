<?php

namespace App\Http\Middleware\Api\Users;

use Closure;

class AdminMiddleware
{
  public function handle($request, Closure $next)
  {
    if (!$request->user()->admin) {
      return response()->json(['message' => 'Unauthorized'], 401);
    }

    return $next($request);
  }
}
