<?php

namespace App\Http\Middleware\Api\Users;

use Closure;

class ActivatedMiddleware
{
  public function handle($request, Closure $next)
  {
    if (!$request->user()->activated) {
      return response()->json(['message' => 'Unauthorized'], 401);
    }

    return $next($request);
  }
}
