<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;


class AuthServiceProvider extends ServiceProvider
{
  protected $policies = [
    'App\Model' => 'App\Policies\ModelPolicy',
  ];

  public function boot()
  {
    $this->registerPolicies();

    Passport::routes();
    Passport::tokensExpireIn(now()->addHours(3));
    Passport::refreshTokensExpireIn(now()->addHours(5));
    Passport::personalAccessTokensExpireIn(now()->addHours(3));
  }
}
